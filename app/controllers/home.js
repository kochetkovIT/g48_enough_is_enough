var express = require('express'),
  router = express.Router(),
  db = require('../models');

module.exports = function (app) {
  app.use('/', router);
};

router.get('/', function (req, res, next) {
  db.Article.findAll().then(function (articles) {
    res.render('index', {
      title: 'Generator-Express MVC',
      articles: articles
    });
  });
});

router.get('/unlock', function (req, res, next) {
  db.Lock.findById(1).then(function(lock){
    if((lock && lock.force) || req.query.level > 0.5) {
      lock.force = 0;
      lock.save();
      res.send('open');
    } else {
      res.send('close');
    }
  });
});

router.get('/force', function (req, res, next) {
  db.Lock.findById(1).then(function(lock){
    lock.force = 1;
    lock.save();
    res.send('done');
  });
});
