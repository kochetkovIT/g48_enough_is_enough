// Example model


module.exports = function (sequelize, DataTypes) {

  var Lock = sequelize.define('Lock', {
    force: DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function (models) {
        // example on how to add relations
        // Article.hasMany(models.Comments);
      }
    }
  });

  return Lock;
};

